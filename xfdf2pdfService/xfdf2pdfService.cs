﻿using BIG.ConfigHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace xfdf2pdf
{
    partial class xfdf2pdfService : ServiceBase
    {

        private System.Timers.Timer timer;
        private bool working = false;
        private PdfFormsUnprocessedDataContext pdfDb;

        private xfdf2pdfSettings settings;

        public xfdf2pdfService()
        {
            InitializeComponent();

            Logger.log.Info("Setting up autoconfig...");
            try
            {
                this.settings = new xfdf2pdfSettings();
                Logger.log.Info("   ...config file found:" + this.settings.Config.FilePath);

            }
            catch (Exception ex)
            {
                Logger.log.Error("failed", ex);
                return;
            }

            //init timer
            this.timer = new System.Timers.Timer();
            timer.Interval = this.settings.PollingInterval * 1000 * 60;
            this.timer.Elapsed += ProcessRecords;

            this.LogSetting();
        }

        protected override void OnStart(string[] args)
        {
            this.StartPolling();

            //do processing on startup
            this.ProcessRecords(null, null);
        }

        protected override void OnStop()
        {
            this.timer.Stop();
        }


        private void LogSetting()
        {
            Logger.log.Info("");
            Logger.log.Info("Settings:");
            Logger.log.Info("");
            Logger.log.Info("   Config file: " + this.settings.Config.FilePath);

            foreach (string allKey in this.settings.AppSettings.Settings.AllKeys)
                Logger.log.Info(string.Format("   {0}: {1}", (object)allKey, (object)this.settings.AppSettings.Settings[allKey].Value));

            Logger.log.Info("");
        }


        public void StartPolling()
        {
            this.timer.Start();
        }


        public void ProcessRecords(object sender, System.Timers.ElapsedEventArgs e)
        {
            Logger.log.Info("Timer tick, processing records...");

            if (this.working)
            {
                Logger.log.Info("   ...can't process records at this point, process is currently working");
                return;
            }

            this.working = true;
            List<PDFSubmit> pdfs = null;

            try
            {
                pdfs = this.GetSubmittedPdfs().ToList();
                Logger.log.Info(string.Format("   found {0} records to process", pdfs.Count().ToString()));
            }
            catch (Exception ex)
            {
                Logger.log.Error("Unable to get the records from db", ex);
            }

            if (pdfs != null && pdfs.Count() != 0)
                this.ProcessPDFs(ref pdfs);


            pdfs = null;
            this.pdfDb = null;

            Logger.log.Info("Back to stand by...");
            Logger.log.Info(""); ;
            this.working = false;
        }

        /// <summary>
        /// Retrieves the submitted/unprocessed PDFS from the database
        /// </summary>
        /// <returns></returns>
        IEnumerable<PDFSubmit> GetSubmittedPdfs()
        {
            Logger.log.Info("Getting the records from the database...");

            this.pdfDb = new PdfFormsUnprocessedDataContext();
            IEnumerable<PDFSubmit> results = pdfDb.ExecuteQuery<PDFSubmit>(this.settings.PDFsQuery);
            return results;
            /*
            return (from p in pdfDb.PDFSubmits
                    where p.Completed == null
                    && p.SavedPDFFile == null
                    select p).ToList();
             */
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdfs"></param>
        void ProcessPDFs(ref List<PDFSubmit> pdfRecords)
        {
            Logger.log.Info("Processing records/pdfs...");

            foreach (var pd in pdfRecords)
            {
                try
                {
                    Logger.log.Info("Processing record id: " + pd.ID);
                    xfdf2pdf x = new xfdf2pdf();

                    string outFileName = String.Format(this.settings.OutputFileNameFormat, pd.ID);

                    var templateFile = Path.Combine(this.settings.TemplateFilesPath, pd.ID_Orig + ".pdf");
                    if (!File.Exists(templateFile))
                    {
                        templateFile = Path.Combine(this.settings.TemplateFilesPath, pd.ID_Override + ".pdf");
                        if (!File.Exists(templateFile))
                            throw new Exception("The template file was not found for both original and override ids");
                    }

                    x.fillXfdf(templateFile,
                                Encoding.ASCII.GetBytes(pd.Content),
                                Path.Combine(this.settings.OutputFilesPath, outFileName));

                    pd.FormCreated = DateTime.Now;
                    pd.SavedPDFFile = outFileName;
                    pd.LastError = null;
                }
                catch (Exception ex)
                {
                    Logger.log.Error("Unable to process the record, moving onto next record", ex);
                    pd.LastError = ex.Message;
                }
            }

            Logger.log.Info("Finished processing all records");

            try
            {
                if (this.settings.AllowDbUpdates) { 
                    Logger.log.Info("Updating database...");
                    this.pdfDb.SubmitChanges();
                    Logger.log.Info("   ...done");
                }
                else
                    Logger.log.Info("No updates to database will ocurr");
            }
            catch (Exception ex)
            {
                Logger.log.Error("Unable to update the database", ex);
            }
        }
    }
}
