﻿using BIG.ConfigHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xfdf2pdf
{
    /// <summary>
    /// Autoconfigurable setting file
    /// </summary>
    class xfdf2pdfSettings : BIG.ConfigHelper.SelfConfigurableClass
    {


        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public int PollingInterval { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string PDFsQuery { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string TemplateFilesPath { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string OutputFilesPath { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string OutputFileNameFormat { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public bool AllowDbUpdates { get; set; }

    }
}
