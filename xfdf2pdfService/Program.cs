﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace xfdf2pdf
{
    class Program
    {
        static void Main(string[] args)
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            Logger.log.Info("xfdfspdf is starting...");
            
            if (Environment.UserInteractive)
            {
                Logger.log.Info("   ...program running as an executable");

                var x = new xfdf2pdfService();
                x.ProcessRecords(null, null);
            }
            else
            {
                Logger.log.Info("   ...program running as a service");

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new xfdf2pdfService()
                };

                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
