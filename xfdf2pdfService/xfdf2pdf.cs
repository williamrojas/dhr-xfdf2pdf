﻿using iTextSharp.text.pdf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace xfdf2pdf
{
    public class xfdf2pdf
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdfTemplateFileName"></param>
        /// <param name="xfdfContents"></param>
        /// <param name="outputFileName"></param>
        public void fillXfdf(String pdfTemplateFileName, byte[] xfdfContents, String outputFileName)
        {
            Logger.log.Info("Setting up xfdf reader and contents");
            XfdfReader xfdf = new XfdfReader(xfdfContents);

            Logger.log.Info("Setting up pdf form");
            using (PdfReader reader = new PdfReader(pdfTemplateFileName))
            {
                // We create an OutputStream for the new PDF
                using (FileStream baos = new FileStream(outputFileName, FileMode.CreateNew))
                {
                    Logger.log.Info("Creating the pdf...");
                    using (PdfStamper stamper = new PdfStamper(reader, baos))
                    {
                        // We alter the fields of the existing PDF
                        stamper.AcroFields.SetFields(xfdf);

                        // take away all interactivity
                        //stamper.FormFlattening = true;    //cant do this with xfa files, need to buy plugin
                    }
                    Logger.log.Info("   ...done");
                }
            }
        }


        
        public bool Print(string file, string printer)
        {
            try
            {
                Process.Start(
                   Registry.LocalMachine.OpenSubKey(
                        @"SOFTWARE\Microsoft\Windows\CurrentVersion" +
                        @"\App Paths\AcroRd32.exe").GetValue("").ToString(),
                   string.Format("/h /t \"{0}\" \"{1}\"", file, printer));
                return true;
            }
            catch (Exception ex){
                Logger.log.Error("", ex);
            }
            return false;
        }


    }
}
